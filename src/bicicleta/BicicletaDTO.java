/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package bicicleta;

/**
 *
 * @author Amanda
 */
public class BicicletaDTO {

   
    
     private Integer bicReferencia;
     private Integer fabCodigo;
     private Integer bicPrecio;
     private Integer bicAnioConstruccion;
    

     public Integer getBicReferencia() {
        return bicReferencia;
    }

    public void setBicReferencia(Integer bicReferencia) {
        this.bicReferencia = bicReferencia;
    }

    public Integer getFabCodigo() {
        return fabCodigo;
    }

    public void setFabCodigo(Integer fabCodigo) {
        this.fabCodigo = fabCodigo;
    }

    public Integer getBicPrecio() {
        return bicPrecio;
    }

    public void setBicPrecio(Integer bicPrecio) {
        this.bicPrecio = bicPrecio;
    }

    public Integer getBicAnioConstruccion() {
        return bicAnioConstruccion;
    }

    public void setBicAnioConstruccion(Integer bicAnioConstruccion) {
        this.bicAnioConstruccion = bicAnioConstruccion;
    }
}
