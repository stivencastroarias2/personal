/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package bicicleta;

import conexion.Conexion;
import java.sql.*;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author User
 */
public class Bicicleta {
    public static Conexion con;
    public static Scanner teclado;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        con = new Conexion();
        teclado = new Scanner(System.in);
        
        //String sql = "INSERT INTO tbl_proveedor_motor(pro_codigo, pro_nombre, pro_direccion, pro_telefono) VALUES (106, 'General Electric', 'calle 29 No. 26-12', '05717239919');";
        //con.ejecutarSentencia(sql);
        //agregarFabricante();
        //obtenerFabricantes();
        
        while (true) {
            System.out.println("Ingrese la opcion del menu: ");
            System.out.println("""
                            1. Agregar bicicleta
                            2. Mostrar bicicletas
                            3. Editar bicicleta
                            4. Eliminar bicicleta
                            5. Salir""");

            int opcion = Integer.parseInt(teclado.nextLine());
            
            if (opcion == 1){
                agregarBicicleta();
            } else if(opcion == 2){
                obtenerBicicletas();
            } else if(opcion == 3){
                actualizarBicicleta();
            } else if (opcion == 4){
                eliminarBicicleta();
            } else if(opcion == 5){
                break;
            }
        }  
    }
    
    
    //CREAR
    public static void agregarBicicleta (){ 
        String sql = "INSERT INTO tbl_bicicleta(fab_codigo, bic_precio, bic_anio_construccion) VALUES (?, ?, ?);";
        try {
            PreparedStatement statement = con.con.prepareStatement(sql);
            
            System.out.println("Agregar Bicicleta");
            
            System.out.println("Ingrese el nit del fabricante: ");
            int nit = Integer.parseInt(teclado.nextLine());
            
            System.out.println("Ingrese el precio: ");
            String bicPrecio = teclado.nextLine();
            
            System.out.println("Ingrese el año de construccion: ");
            String bicAnioConstruccion = teclado.nextLine();
           
            
            statement.setInt(1, nit);
            statement.setInt(2, Integer.parseInt(bicPrecio)) ;
            statement.setInt(3,  Integer.parseInt(bicAnioConstruccion));
            
            statement.executeUpdate();
            
            System.out.println("Se han guardado datos correctamente");

        } catch (SQLException ex) {
            Logger.getLogger(Bicicleta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    //ACTUALIZAR
    public static void actualizarBicicleta(){ 
        String sql = "UPDATE tbl_bicicleta SET fab_codigo=?, bic_precio=?, bic_anio_construccion=?  WHERE bic_referencia=?";
        try {
            PreparedStatement statement = con.con.prepareStatement(sql);
            
            System.out.println("Agregar Bicicleta");
            
            System.out.println("Ingrese el nit del fabricante: ");
            //teclado.nextLine()
            int nit = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el nit del fabricante: "));
            
            System.out.println("Ingrese el precio: ");
            String bicPrecio = teclado.nextLine();
            
            System.out.println("Ingrese el año de construccion: ");
            String bicAnioConstruccion = teclado.nextLine();
            
            System.out.println("Ingrese la referencia de la bicicleta: ");
            String bicReferencia = teclado.nextLine();
            
            statement.setInt(1, nit);
            statement.setInt(2, Integer.parseInt(bicPrecio));
            statement.setInt(3, Integer.parseInt(bicAnioConstruccion));
            statement.setInt(4, Integer.parseInt(bicReferencia));
            
            statement.executeUpdate();
            
            System.out.println("Se han actualizado datos correctamente");
        } catch (SQLException ex) {
            Logger.getLogger(Bicicleta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    //ELIMINAR
    public static void eliminarBicicleta(){ 
        String sql = "DELETE FROM tbl_bicicleta WHERE bic_referencia = ? ";
        try {
            PreparedStatement statement = con.con.prepareStatement(sql);
            
            System.out.println("Ingrese el codigo de la bicicleta que desea eliminar: ");
            int codigo = Integer.parseInt(teclado.nextLine());
            
            statement.setInt(1, codigo);
            statement.executeUpdate();
            
             System.out.println("Se ha eliminado datos correctamente");
        } catch (SQLException ex) {
            Logger.getLogger(Bicicleta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    //CONSULTA
    public static void obtenerBicicletas (){ 
        String sql = "SELECT * FROM tbl_bicicleta";
        try {
            Statement statement = con.con.createStatement();
            ResultSet resultado = statement.executeQuery(sql);
            
            while (resultado.next()){
                int bicReferencia = resultado.getInt(1);
                int fabCodigo = resultado.getInt(2);
                int bicPrecio = resultado.getInt(3);
                int bicAnioConstruccion = resultado.getInt(4);
                
                System.out.println("Referencia: " + bicReferencia);
                System.out.println("Codigo del fabricante: " + fabCodigo);
                System.out.println("Precio: " + bicPrecio);
                System.out.println("Año de construccion: " + bicAnioConstruccion);
                System.out.println("\n");
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(Bicicleta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
