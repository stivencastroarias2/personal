/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package conexion;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author User
 */
public class Conexion {
    String dbUrl = "jdbc:mysql://localhost:3306/prueba5";
    String nombre = "prueba5";
    String contrasenia = "admin";
    public Connection con;

    public Conexion() {
        try
        {
            con = DriverManager.getConnection(dbUrl, nombre, contrasenia);
            if (con != null) {
                System.out.println("CONECTADO!!!");
            } else {
                System.out.println("NO CONECTADO!!!");
            }
            
        } catch (SQLException ex)
        {
            System.out.println("NO CONECTADO, EXC!!!");
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void ejecutarSentencia(String sql) {
        try
        {
            PreparedStatement statament = this.con.prepareStatement(sql);
            statament.executeUpdate();
        } catch (SQLException ex)
        {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }  
}
